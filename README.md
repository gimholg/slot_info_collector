mem

Speed: 133-266 DDR
Speed: 400-800 DDR2
Speed: 1066-1600 DDR3
Speed: 1866-3200 DDR4

| Memory Type/SMBIOSMemoryType | RAM Type         |
| ---------------------------- | ---------------- |
| 0                            | Unknown          |
| 1                            | Other            |
| 2                            | DRAM             |
| 3                            | Synchronous DRAM |
| 4                            | Cache DRAM       |
| 5                            | EDO              |
| 6                            | EDRAM            |
| 7                            | VRAM             |
| 8                            | SRAM             |
| 9                            | RAM              |
| 10                           | ROM              |
| 11                           | Flash            |
| 12                           | EEPROM           |
| 13                           | FEPROM           |
| 14                           | EPROM            |
| 15                           | CDRAM            |
| 16                           | 3DRAM            |
| 17                           | SDRAM            |
| 18                           | SGRAM            |
| 19                           | RDRAM            |
| 20                           | DDR              |
| 21                           | DDR2             |
| 22                           | DDR2 FB-DIMM     |
| 24                           | DDR3             |
| 25                           | FBD2             |
| 26                           | DDR4             |

| FormFactor   | RAM Type    |
| ------------ | ----------- |
| 0            | Unknown     |
| 1            | Other       |
| 2            | SIP         |
| 3            | DIP         |
| 4            | ZIP         |
| 5            | SOJ         |
| 6            | Proprietary |
| 7            | SIMM        |
| 8            | DIMM        |
| 9            | TSOP        |
| 10           | PGA         |
| 11           | RIMM        |
| 12           | SODIMM      |
| 13           | SRIMM       |
| 14           | SMD         |
| 15           | SSMP        |
| 16           | QFP         |
| 17           | TQFP        |
| 18           | SOIC        |
| 19           | LCC         |
| 20           | PLCC        |
| 21           | BGA         |
| 22           | FPBGA       |
| 23           | LGA         |
| 24           | FB-DIMM     |
| **34** | DDR5        |

类型 参数 含义
uint64 Capacity 获取内存容量（单位KB）
string Caption 物理内存还虚拟内存
uint32 ConfiguredClockSpeed 配置时钟速度
uint32 ConfiguredVoltage 配置电压
string CreationClassName 创建类名（就是更换这个类的名字）
uint16 DataWidth 获取内存带宽
string Description 描述更Caption一样
string DeviceLocator 获取设备定位器
uint16 FormFactor 构成因素
boolean HotSwappable 是否支持热插拔
datetime InstallDate 安装日期（无值）
uint16 InterleaveDataDepth 数据交错深度
uint32 InterleavePosition 交错的位置
string Manufacturer 生产商
uint32 MaxVoltage 最大电压
uint16 MemoryType 内存类型
uint32 MinVoltage 最小电压
string Model 型号
string Name 名字
string OtherIdentifyingInfo 其他识别信息
string PartNumber 零件编号
uint32 PositionInRow 行位置
boolean PoweredOn 是否接通电源
boolean Removable 是否可拆卸
boolean Replaceable 是否可更换
string SerialNumber 编号
string SKU SKU号
uint32 SMBIOSMemoryType SMBIOS内存类型
uint32 Speed 速率
string Status 状态
string Tag 唯一标识符的物理存储器
uint16 TotalWidth 总宽
uint16 TypeDetail 类型详细信息
string Version 版本信息
